package com.zb.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.zb.dao.TestDao;
import com.zb.entity.Test;
import com.zb.service.TestService;

@Controller
public class Testcontroller {
	
	@Autowired
	private TestService testService;
	@RequestMapping("show.do")
	@ResponseBody
	public String show(){
		List<Test> i =testService.fin();
		return JSONObject.toJSONString(i);
	} 
}
