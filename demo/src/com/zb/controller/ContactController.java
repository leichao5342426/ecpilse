package com.zb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.zb.entity.Contact;
import com.zb.service.ContactService;

@Controller
public class ContactController {
	@Autowired
	private	ContactService contactService; 
	
		//@RequestMapping 来映射URL 到控制器类
		/**
		 * @ResponseBody是作用在方法上的，@ResponseBody 表示该方法的返回结果直接写入 HTTP response body 中，
		 * 一般在异步获取数据时使用【也就是AJAX】，在使用 @RequestMapping后，返回值通常解析为跳转路径，
		 * 但是加上 @ResponseBody 后返回结果不会被解析为跳转路径，而是直接写入 HTTP response body 中。
		 * 比如异步获取 json 数据，加上 @ResponseBody 后，会直接返回 json 数据。@RequestBody
		 *  将 HTTP 请求正文插入方法中，使用适合的 HttpMessageConverter 将请求体写入某个对象。
		 * @return
		 */
		@RequestMapping(value="ajaxShowContact.do",produces = "text/html;charset=UTF-8")
		@ResponseBody
		public String ajaxShowContact(){
			List<Contact> list= contactService.findAll();
			return JSONObject.toJSONString(list);
			
		}
		@RequestMapping(value="add.do",produces = "text/html;charset=UTF-8")
		public String add(Contact c){
			contactService.insert(c);
			return "redirect:/index.jsp";
		}	
		@RequestMapping("findBy.do")
		public String findBy(Integer id){
			int c=contactService.deleteByPrimaryKey(id);
			return "index.jsp";
}
		
		@RequestMapping("findByI.do")
		public String findById(int id,Model model){
		Contact c=	contactService.selectByPrimaryKey(id);
			model.addAttribute("c", c);
			
			return "update.jsp"; 
			
		}
		
	
		@RequestMapping("update.do")
		public String update(Contact c){
		contactService.updateByPrimaryKeySelective(c);
		return "index.jsp";
	}
		@RequestMapping("search.do")
		@ResponseBody
		public String search(String name){
		List<Contact> list=	contactService.search(name);
			return JSONObject.toJSONString(list);
		}
		
}
