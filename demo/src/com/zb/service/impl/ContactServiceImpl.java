package com.zb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.dao.ContactMapper;
import com.zb.entity.Contact;
import com.zb.service.ContactService;

@Service
@Transactional
public class ContactServiceImpl implements ContactService{
	@Autowired
	private ContactMapper contactMapper;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return contactMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Contact record) {
		// TODO Auto-generated method stub
		return contactMapper.insert(record);
	}

	@Override
	public int insertSelective(Contact record) {
		// TODO Auto-generated method stub
		return contactMapper.insertSelective(record);
	}

	@Override
	public Contact selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return contactMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Contact record) {
		// TODO Auto-generated method stub
		return contactMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Contact record) {
		// TODO Auto-generated method stub
		return contactMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Contact> findAll() {
		// TODO Auto-generated method stub
		return contactMapper.findAll();
	}

	@Override
	public List<Contact> search(String name) {
		// TODO Auto-generated method stub
		return contactMapper.search(name);
	}

	

}
