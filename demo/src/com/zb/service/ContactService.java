package com.zb.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.entity.Contact;

	
public interface ContactService {
	 int deleteByPrimaryKey(Integer id);

	    int insert(Contact record);

	    int insertSelective(Contact record);

	    Contact selectByPrimaryKey(Integer id);

	    int updateByPrimaryKeySelective(Contact record);

	    int updateByPrimaryKey(Contact record);
	    
	    
	    public List<Contact> findAll();
	    List<Contact> search(@Param("name")String name);

}
