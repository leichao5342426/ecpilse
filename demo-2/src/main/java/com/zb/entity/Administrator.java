package com.zb.entity;

public class Administrator {
/**
 * 这个是管理员
 * 
 */
	private int id;
	//密码
	private  String password;
	//电话号码
	private String phone;
	//管理人
	private String Administrator;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAdministrator() {
		return Administrator;
	}
	public void setAdministrator(String administrator) {
		Administrator = administrator;
	}
	
	
}
