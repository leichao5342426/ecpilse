package com.zb.dao;

import java.util.Map;

import com.zb.entity.Administrator;

public interface AdministratorDao {
	/**
	 * 登陆验证的方法
	 * @param map username:用户名，userpassword：用户密码
	 * @return
	 */
	public abstract  Administrator doLogin(Map<String, Object> map);
}
