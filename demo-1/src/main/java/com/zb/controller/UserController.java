package com.zb.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zb.entity.User;
import com.zb.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@RequestMapping("doLogin.do")
	public String doLogin(HttpSession session, String name,
			String password, Model model) {
		
		User user = userService.doLogin(name, password);

		if (user != null) {
			// ��½�ɹ�
			session.setAttribute("loginUser", user);
			return "test.jsp";
		} else {
			// ��½ʧ��
			model.addAttribute("loginFlag", "error");
			return "forward:login.jsp";
		}
	}
		
	
}
