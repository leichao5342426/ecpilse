package com.zb.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.dao.UserDao;
import com.zb.entity.User;
import com.zb.service.UserService;

@Service
@Transactional
public class UserServiceImpl  implements UserService{
	@Autowired
	private UserDao dao;
	public User doLogin(String name, String password) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		map.put("password", password);
		User user = dao.doLogin(map);
		return user;
	}
}