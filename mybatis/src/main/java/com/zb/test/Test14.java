package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import com.zb.dao.HouseDao;
import com.zb.entity.House;

public class Test14 {
	public static void main(String[] args) {
		try {
			InputStream inputStream = Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession session = sqlSessionFactory.openSession();
			HouseDao dao = session.getMapper(HouseDao.class);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("address", "��");
			param.put("minprice", 300);
			param.put("maxprice", 501);
			List<House> list = dao.searchHouse2(param);
			for (House h : list) {

				System.out.println(h.getAddress() + "  " + h.getPrice());
			}
			session.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
