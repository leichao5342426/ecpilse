package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UsersDao;
import com.zb.entity.Users;

/**
 * 二级缓存： 作用在sessionFacatory上 可以被所有的session共享 开启二级缓存的步骤
 *
 * @author lc
 *
 */
public class TestCache2 {

	public static void main(String[] args) {
		try {
			InputStream inputStream = Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession session = sessionFactory.openSession();

			UsersDao dao = session.getMapper(UsersDao.class);
			List<Users> u = dao.finAll("超");
			for (Users users : u) {
				System.out.println(users.getId());
			}
			session.close();
			SqlSession session1 = sessionFactory.openSession();
			UsersDao dao1=session1.getMapper(UsersDao.class);
			List<Users> s=dao1.finAll("超");
			for (Users users : s) {
				System.out.println(users.getName());
			}
			session.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
