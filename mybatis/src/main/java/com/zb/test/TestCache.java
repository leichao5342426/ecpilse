package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UsersDao;
import com.zb.entity.Users;

/**
 * 一级缓：
 * 作用在一个session中
 * 如果在session重复获取某条或者几条数据  那么只会在第一次数据库发起查询  以后直接冲缓存中获取这个数据
 * 
 * @author lc
 *
 */
public class TestCache {
public static void main(String[] args) {
	try {
		InputStream inputStream=Resources.getResourceAsStream("mybatis.cfg.xml");
		SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
		SqlSession session=sessionFactory.openSession();
		
		UsersDao  dao =session.getMapper(UsersDao.class);
		List<Users> u=dao.finAll("超");
		for (Users users : u) {
			System.out.println(users.getId());
		}
				
session.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
