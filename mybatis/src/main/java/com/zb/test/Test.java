package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UsersDao;
import com.zb.entity.Users;

public class Test {
	public static void main(String[] args) {
		String resource = "mybatis.cfg.xml";
		// 就是通过ClassLoader的方法将传入资源文件解析为指定流或者其它
		SqlSession sqlSession = null;
		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			// SqlSession 完全包含了面向数据库执行 SQL 命令所需的所有方法
			sqlSession = sqlSessionFactory.openSession();
			UsersDao dao = sqlSession.getMapper(UsersDao.class);
			List<Users> u = dao.finAll("超");
			for (Users users : u) {
				System.out.println(users.getId());
				System.out.println(users.getName());
				System.out.println();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
	}
}
