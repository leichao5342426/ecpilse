package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UsersDao;
import com.zb.entity.House;
import com.zb.entity.Users;

public class Test9 {
public static void main(String[] args) {
	String resource ="mybatis.cfg.xml";
	
	try {
		InputStream inputStream=Resources.getResourceAsStream(resource);
		SqlSessionFactory sessionFactory= new SqlSessionFactoryBuilder().build(inputStream);
		SqlSession session=sessionFactory.openSession();
		UsersDao dao=session.getMapper(UsersDao.class);
		List<Users> list=dao.find();
		for (Users u : list) {
			System.out.println(u.getId());
		}
		session.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
