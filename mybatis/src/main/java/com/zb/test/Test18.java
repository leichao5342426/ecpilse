package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import com.zb.dao.HouseDao;
import com.zb.entity.House;

public class Test18 {
	public static void main(String[] args) {
		try {
			InputStream inputStream = Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession session = sqlSessionFactory.openSession();
			HouseDao dao = session.getMapper(HouseDao.class);
			
			List<String> arr=new ArrayList<String>();
			arr.add("上海");
			arr.add("老山");
			arr.add("南京");
			List<House> list=dao.searchHouse4(arr);
			for (House house : list) {
				System.out.println(house.getAddress());
			}
			session.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
