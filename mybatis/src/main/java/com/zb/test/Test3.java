package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UsersDao;
import com.zb.entity.Users;

public class Test3 {
public static void main(String[] args) {
	String resouce="mybatis.cfg.xml";
	try {
		InputStream inputStream=Resources.getResourceAsStream(resouce);
		SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
		SqlSession session=sqlSessionFactory.openSession();
		UsersDao dao=session.getMapper(UsersDao.class);
		
		List<Users> list=dao.findHouse();
		for (Users users : list) {
			System.out.println(users.getAge()+users.getName()+users.getHousetitle());
		}
		session.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}
