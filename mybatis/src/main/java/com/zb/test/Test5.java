package com.zb.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UsersDao;
import com.zb.entity.Users;

public class Test5 {
	public static void main(String[] args) {
		String resouce = "mybatis.cfg.xml";

		SqlSession session;

		try {
			InputStream inputStream = Resources.getResourceAsStream(resouce);
			SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
			session = sqlSessionFactory.openSession();
			UsersDao dao = session.getMapper(UsersDao.class);

			Users u = new Users();
			u.setName("c");
			u.setAddress("b");
			int count = dao.updateUsers(u);
			System.out.println(count);
			session.commit();

			session.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
