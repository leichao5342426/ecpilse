package com.zb.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import com.zb.dao.UsersDao;

public class Test7 {
public static void main(String[] args) {
	String resouce = "mybatis.cfg.xml";

	 

	try {
		InputStream inputStream = Resources.getResourceAsStream(resouce);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		SqlSession session = sqlSessionFactory.openSession();
		UsersDao dao = session.getMapper(UsersDao.class);

		
		int count = dao.deleteUsers(10);
		
		System.out.println(count);
		session.commit();

		session.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
}

