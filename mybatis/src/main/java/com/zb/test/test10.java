package com.zb.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.HouseDao;
import com.zb.entity.House;

public class test10 {
public static void main(String[] args) {
	try {
		InputStream inputStream=Resources.getResourceAsStream("mybatis.cfg.xml");
		SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
		SqlSession session=sqlSessionFactory.openSession();
		
		HouseDao dao=session.getMapper(HouseDao.class);
		List<House> list=dao.find();
		for (House h : list) {
			System.out.println(h.getId()+"   "+h.getTitle()+"   "+h.getPrice());
			System.out.println(h.getUsers().getId()+h.getUsers().getName()+h.getUsers().getAge()+"  "+h.getUsers().getName());
			
		}
		session.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}
}
