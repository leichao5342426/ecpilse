package com.zb.entity;

import java.util.List;

public class Users {
	private int id;
	private String name;
	private int age;
	private String address;
	private String housetitle;
	
	/**
	 * 一对多的解决方案
	 * 弄清楚 主从关系
	 * 在主表中对应的实体类中  增加 对应的实体类的集合对象
	 * 配置
	 * 
	 */
	private List<House> houses; 
	

	public List<House> getHouses() {
		return houses;
	}

	public void setHouses(List<House> houses) {
		this.houses = houses;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getHousetitle() {
		return housetitle;
	}

	public void setHousetitle(String housetitle) {
		this.housetitle = housetitle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
