package com.zb.entity;

public class House {
private int id;
private String title;
private double price;
private String address;
private Users users;

public Users getUsers() {
	return users;
}
public void setUsers(Users users) {
	this.users = users;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}


}
