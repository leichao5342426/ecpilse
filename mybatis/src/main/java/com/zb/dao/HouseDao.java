package com.zb.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.zb.entity.House;

public interface HouseDao {
	/**
	 * 两表查询
	 * 使用一对一的实现方式
	 * 
	 * @return
	 */
	public List<House> find();
	
	/**
	 * 动态sql
	 * @param param
	 * @return
	 */
	public List<House> searchHouse1(Map<String,Object>param);
	
	public List<House> searchHouse2(Map<String,Object>param);
	
	public int updateHouse(House house);
	
	public int updateHouse1(House house);
	
	public List<House> searchHouse3(String[] address);
	public List<House> searchHouse4(List address);
	public List<House> searchHouse5(@Param("type")int type);
	
	
	
}

