package com.zb.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.zb.entity.Users;

public interface UsersDao {
	// 模糊查询
	public List<Users> finAll(String name);
	// 如果你的映射方法的形参有多个，
	// 这个注解使用在映射方法的参数上就能为它们取自定义名字。
	// 若不给出自定义名字，多参数（不包括 RowBounds 参数）则先以 "param" 作前缀，
	// 再加上它们的参数位置作为参数别名。例如 #{param1}, #{param2}，这个是默认值。
	// 如果注解是 @Param("person")，那么参数就会被命名为 #{person}。
	public List<Users> findByCondition(@Param("name") String name, @Param("minage") int minage);
	public List<Users> findByCondition1(Map<String, Object> param);
	public List<Users> findHouse();
	// 保存
	public int saveUsers(Users u);
	// 修改
	public int updateUsers(Users u);
	public int deleteUsers(int id);
	// 一对多
	public List<Users> find();
}
