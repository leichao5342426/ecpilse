<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="en">
    <title>超市订单管理系统</title>
    <link type="text/css" rel="stylesheet" href="statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="statics/css/public.css" />
    <script type="text/javascript" src="statics/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
      	function showTime(){
    		var d = new Date();
    		var str = d.getFullYear()+"年"+(d.getMonth()+1)+"月"+d.getDate()+"日 "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    		var day = d.getDay();
    		switch(day){
    			case 0:
    				str+="星期天"
    				break;
    			case 1:
    				str+="星期一"
    				break;
    			case 2:
    				str+="星期二"
    				break;
    			case 3:
    				str+="星期三"
    				break;
    			case 4:
    				str+="星期四"
    				break;
    			case 5:
    				str+="星期五"
    				break;
    			case 6:
    				str+="星期六"
    				break;
    		}
    		$("#time").html(str);
    	}
    	
    	var f = function(){setInterval("showTime()",1000)}
    	
    	f();
    </script>
</head>

<body>
	<header class="publicHeader">
        <h1>超市订单管理系统</h1>
        <div class="publicHeaderR">
        <!-- sessionScope整体的意思是获得存放在session.setAttrbute(key,value)的值即session.getAttribute(key); -->
            <p><span>下午好！</span><span style="color: #fff21b">${sessionScope.loginUser.username }</span> , 欢迎你！</p>
            <a href="LoginOutServlet">退出</a>
        </div>
    </header>
<!--时间-->
    <section class="publicTime">
        <span id="time">2015年1月1日 11:11  星期一</span>
        <a href="#">温馨提示：为了能正常浏览，请使用高版本浏览器！（IE10+）</a>
    </section>
 <!--主体内容-->
 <section class="publicMian ">
     <div class="left">
         <h2 class="leftH2"><span class="span1"></span>功能列表 <span></span></h2>
         <nav>
             <ul class="list">
                  <li ><a href="list.html">订单管理</a></li>
				  <li><a href="list.html">供应商管理</a></li>
				  <li><a href=ToUserListServlet>用户管理</a></li>
				  <li><a href="">角色管理</a></li>
				  <li><a href="">密码修改</a></li>
				  <li><a href="LoginOutServlet">退出系统</a></li>
             </ul>
         </nav>
     </div>