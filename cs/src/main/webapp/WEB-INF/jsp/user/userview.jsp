<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
	<%@include file="../header.jsp" %>

<div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>用户管理页面 >> 用户信息查看页面</span>
        </div>
        <div class="providerView">
            <p><strong>用户编号：</strong><span>${requestScope.user.usercode }</span></p>
            <p><strong>用户名称：</strong><span>${requestScope.user.username }</span></p>
            <p><strong>用户性别：</strong>
            	<span>
 					<c:if test="${requestScope.user.gender==0 }">男</c:if>
 					<c:if test="${requestScope.user.gender==1 }">女</c:if>
				</span>
			</p>
            <p><strong>出生日期：</strong><span><fmt:formatDate value="${requestScope.user.birthday}" pattern="yyyy-MM-dd"/></span></p>
            <p><strong>用户电话：</strong><span>${requestScope.user.phone }</span></p>
            <p><strong>用户地址：</strong><span>${requestScope.user.address}</span></p>
            <p><strong>用户角色：</strong><span>${requestScope.user.role.rolename}</span></p>            
            <p><strong>个人证件照：</strong><span>
           
            	 	<img alt="" width="300px" height="300px" src="${requestScope.user.cardphoto }">
          
            </span></p>            
            <p><strong>工作证照片：</strong><span>
          
            	 	<img alt="" width="300px" height="300px" src="${requestScope.user.workphoto }">
            	
            </span></p>
			<div class="providerAddBtn">
            	<input type="button" id="back" name="back" value="返回" >
            </div>
        </div>
    </div>
</section>
	<%@include file="../foot.jsp" %>