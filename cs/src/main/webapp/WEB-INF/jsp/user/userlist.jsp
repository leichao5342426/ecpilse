<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@ page isELIgnored="false" %>
     <%@include file="/WEB-INF/jsp/header.jsp" %>
    <script>
    
    $(function() {
    	bindRole();	bindUser(currentPage);bindEvent();
	})
    var currentPage=1;
    function bindUser(currentPage) {
       <%-- $("#queryname")是id选择器,获得id为queryname的标签,通过.val(),获得这个标签的value的值,赋值给你声明的变量--%>
    	var queryname = $("#queryname").val();
    	var role = $("#role").val();
    	$.ajax({
    				Type:"post",
    				url:"AjaxShowUserListServlet",
    				dataType:"json",
    				contentType: 'text/json,charset=utf-8',
    				data : {"queryname" : queryname,"role" : role,"currentPage" : currentPage},
    				success : function(msg){
    					var str ="";
    					$(msg.list).each(function(i){
    						var u=msg.list[i];
    						str +="<tr>"
    						str +="<td>"
    						str +="<span>"+u.usercode+"</span>"
    						str +="</td>"
    						str +="<td>"
    						str +="<span>"+u.username+"</span>"
    						str +="</td>"
    						str +="<td>"
    						str +="<span>"
    						str	 +=u.gender==0?"男":"女"
    						str	 +="</span>"
    						str	 +="</td>"
    						str	 +="<td>"
    						str	 +="<span>"+u.age+"</span>"
    						str	 +="</td>"
    						str	 +="<td>"
    						str	 +="<span>"+u.phone+"</span>"
    						str	 +="</td>"
    						str	 +="<td>"
    						str	 +="<span>"+u.role.rolename+"</span>"
    						str	 +="</td>"
    						str	 +="<td>"
    						str	 +='<span><a class="viewUser" href="javascript:;"><img src="statics/images/read.png" alt="查看" title="查看"/></a></span>'		
    						str	 +='<span><a class="modifyUser" href="javascript:;"><img src="statics/images/xiugai.png" alt="修改" title="修改"/></a></span>'
    						str	 +='<span><a class="deleteUser" href="javascript:;"><img src="statics/images/schu.png" alt="删除" title="删除"/></a></span>'		
    						str	 +="</td>"
    						str	 +="</tr>"
    						
    					})
    					$(".providerTable tr:not(:first)").remove();
    					$(".providerTable").append(str);
    					$("#count").html(msg.count);
    					$("#currentPage").html(currentPage);
    					$("#totalPage").html(msg.totalPage);
    				}

    	})
    	
	}
     
     function bindRole(){
    	 
    	   $.ajax({
    	          type: "POST",                           //传数据的方式
    	          url: "AjaxShowAllRoleServlet", 	      //servlet地址
    	          dataType : "json",      //数据类型
    	          success: function(msg){       //传数据成功之后的操作 
    	        	var str="";
    	 			$(msg).each(function(i) {
						var r=msg[i];
						str+="<option value='"+r.id+"'>"+r.rolename+"</option>";
					})
					$("#role").append(str);
    	          }
    	      });
    	   }
     
     
		function bindEvent(){
			//跳转到查看
				$(".providerTable").delegate(".viewUser","click",function(){
				var id = $(this).attr("userid");
				window.location = "UserviewServlet?id="+id
			})
			
			$("#searchbutton").click(function(){
				currentPage = 1;
				bindUser(currentPage)
			})
			
			$(".next").click(function(){
				if(currentPage == parseInt($("#totalPage").html())){
					return;
				}
				currentPage++;
				bindUser(currentPage)
			})
			
			$(".prev").click(function(){
				if(currentPage == 1){
					return;
				}
				currentPage--;
				bindUser(currentPage)
			})
			
			$(".first").click(function(){
				currentPage = 1;
				bindUser(currentPage)
			})
			
			$(".last").click(function(){
				currentPage = parseInt($("#totalPage").html())
				bindUser(currentPage)
			})
			
			$(".page-btn").click(function(){
				var inputPage = $("#inputPage").val();
				if(isNaN(inputPage)){
					alert("请正确输入页码数");
					return;
				}
				
				var page  = parseInt(inputPage);
				if(page <= 0){
					alert("页码数必须大于0")
					return;
				}
				
				if(page > parseInt($("#totalPage").html())){
					alert("页码数超过了最大页码数")
					return;
				}
				
				currentPage = page;
				bindUser(currentPage)
				
			})
		}
    	 
     </script>
    
	<div class="right">
            <div class="location">
                <strong>你现在所在的位置是:</strong>
                <span>用户管理页面</span>
            </div>
            <div class="search">
           		<form method="post" action="">
					 <span>用户名：</span>
					 <input name="queryname" class="input-text"	type="text" value="">
					 
					 <span>用户角色：</span>
					 <select id="role" name="queryUserRole">
						   <option value="0">--请选择--</option>
						  
						
	        		</select>
					
					 <input	value="查 询" type="submit" id="searchbutton">
					 <a href="ToUserAddServlet" >添加用户</a>
				</form>
            </div>
            <!--用户-->
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tr class="firstTr">
                    <th width="10%">用户编码</th>
                    <th width="20%">用户名称</th>
                    <th width="10%">性别</th>
                    <th width="10%">年龄</th>
                    <th width="10%">电话</th>
                    <th width="10%">用户角色</th>
                    <th width="30%">操作</th>
                </tr>
                   
					
					
					
					
			</table>
		  	<div class="page-bar">
				<ul class="page-num-ul clearfix">
					<li>共<span id="count"></span>条记录&nbsp;&nbsp; <span id="currentPage"></span>/<span id="totalPage"></span>页</li>
					
						<a href="javascript:void(0)" class="first">首页</a>
						<a href="javascript:void(0)" class="prev">上一页</a>
				
					
						<a href="javascript:void(0)" class="next">下一页</a>
						<a href="javascript:void(0)" class="last">最后一页</a>
					
					&nbsp;&nbsp;
				</ul>
			 <span class="page-go-form"><label>跳转至</label>
		     <input type="text" name="inputPage" id="inputPage" class="page-key" />页
		     <button type="button" class="page-btn" onClick=''>GO</button>
			</span>
			</div> 
        </div>
    </section>

	<!--点击删除按钮后弹出的页面-->
	<div class="zhezhao"></div>
	<div class="remove" id="removeUse">
		<div class="removerChid">
			<h2>提示</h2>
			<div class="removeMain">
				<p>你确定要删除该用户吗？</p>
				<a href="#" id="yes">确定</a>
				<a href="#" id="no">取消</a>
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/jsp/foot.jsp" %>
 