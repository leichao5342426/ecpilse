package com.zb.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.zb.entity.User;

public interface UserDao {
	/**
	 * 登录
	 * @param username
	 * @param userpassword
	 * @return
	 */
	public User doLogin(@Param("username")String username,@Param("userpassword")String userpassword);
	/**
	 * 查询用户
	 * @param param
	 * @return
	 */
	public List<User> listUser(Map<String,Object>param);
	/**
	 * 查询用户记录数
	 * @param param
	 * @return
	 */
	public int countUser(Map<String, Object> param);
	
	/**
	 * 保存用户
	 * @param u
	 * @return
	 */
	public int addUser(User u);
	/**
	 * 根据id查询单个对象
	 * @param id
	 * @return
	 */
	public User finByid(int id);
	
}
