package com.zb.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.zb.entity.User;

public interface UserService {
	public User doLogin(@Param("username")String username,@Param("userpassword")String userpassword);
	public List<User> listUser(String queryname, String role, int currentPage);
	
	public int countUser(String queryname, String role);
	
	/**
	 * 保存用户
	 * @param u
	 * @return
	 */
	public int addUser(User u);
	/**
	 * 根据id查询单个对象
	 * @param id
	 * @return
	 */
	public User finByid(int id);
	
}
