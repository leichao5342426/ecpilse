package com.zb.service.Impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.UserDao;
import com.zb.entity.User;
import com.zb.service.UserService;

public class UserServiceImpl implements UserService{

	public User doLogin(String username, String userpassword) {
		User u= new User();
		SqlSession Session=null;
		try {
			InputStream inputStream=Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
			Session=sqlSessionFactory.openSession();
			UserDao dao=Session.getMapper(UserDao.class);
			u= new User();
			u=dao.doLogin(username, userpassword);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			Session.close();
		}
		
				
		return u;
	}

	public List<User> listUser(String queryname, String role, int currentPage) {
		List<User> list = null;
		try {
			InputStream inputStream=Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession Session=sqlSessionFactory.openSession();
			UserDao dao=Session.getMapper(UserDao.class);
			Map<String,Object> param = new HashMap<String,Object>();
			if(queryname != null && !"".equals(queryname)){
				param.put("queryname", queryname);
			}
			if(!"0".equals(role)){
				param.put("role", role);
			}
			param.put("start", (currentPage - 1) * 4);
			list = dao.listUser(param);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public int countUser(String queryname, String role) {
		// TODO Auto-generated method stub
		int count=0;
		
		try {
			InputStream inputStream=Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession Session=sqlSessionFactory.openSession();
			UserDao dao=Session.getMapper(UserDao.class);
			Map<String,Object> param = new HashMap<String,Object>();
			if(queryname != null && !"".equals(queryname)){
				param.put("queryname", queryname);
			}
			if(!"0".equals(role)){
				param.put("role", role);
			}
			
			count = dao.countUser(param);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}
	public static void main(String[] args) {
		 
	}

	public int addUser(User u) {
		SqlSession session=null;
		int count=0;
		
		try {
			String resource="mybatis.cfg.xml";
			InputStream inputStream=Resources.getResourceAsStream(resource);
			SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
			session=sessionFactory.openSession();
			UserDao dao=session.getMapper( UserDao.class);
			count =dao.addUser(u);
			session.commit();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			session.close();
			
		}
		return count;
		
	}

	public User finByid(int id) {
		SqlSession session=null;
		User u=null;
		try {
			String resource="mybatis.cfg.xml";
			InputStream inputStream=Resources.getResourceAsStream(resource);
			SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
			session=sessionFactory.openSession();
			UserDao dao=session.getMapper( UserDao.class);
			u =dao.finByid(id);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			session.close();
			
		}
		return u;
		
	}


}
