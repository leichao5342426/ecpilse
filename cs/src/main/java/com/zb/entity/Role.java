package com.zb.entity;

import java.util.Date;

/**
 * 角色  对应smbms_role表
 * @author Administrator
 *
 */
public class Role {
    
    private int id;					//id
	
	private String rolecode;		//角色编号
	
	private String rolename;		//角色名
	
	private int createby;			//创建人id
	
	private Date creationdate;		//创建时间
	
	private int modifyby;			//修改人id
	
	private Date modifydate;		//修改时间

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRolecode() {
		return rolecode;
	}

	public void setRolecode(String rolecode) {
		this.rolecode = rolecode;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public int getCreateby() {
		return createby;
	}

	public void setCreateby(int createby) {
		this.createby = createby;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public int getModifyby() {
		return modifyby;
	}

	public void setModifyby(int modifyby) {
		this.modifyby = modifyby;
	}

	public Date getModifydate() {
		return modifydate;
	}

	public void setModifydate(Date modifydate) {
		this.modifydate = modifydate;
	}
	
	
}
