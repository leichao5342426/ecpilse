package com.zb.entity;

import java.util.Date;

public class User {
	private int id;						//id
	 
	private int smbid;					//流水号
	
	private String usercode;			//用户编号
	
	private String username;			//用户名
	
	private String userpassword;		//密码
	
	private int gender;					//性别  0：男  1：女
	 
	private Date birthday;				//生日
	
	private String phone;				//电话号码
	
	private String address;				//地址
	private Role role;					//用户权限
	
	private int userroleid;				//角色id
	
	private int createby;				//创建人id
	
	private Date creationdate;			//创建时间
	
	private int modifyby;				//修改人id
	
	private Date modifydate;			//修改时间
	private int age;			//年龄
	
	
	private String cardphoto;			//证件照的地址
	
	private String workphoto;			//工作照的地址

	public String getCardphoto() {
		return cardphoto;
	}

	public void setCardphoto(String cardphoto) {
		this.cardphoto = cardphoto;
	}

	public String getWorkphoto() {
		return workphoto;
	}

	public void setWorkphoto(String workphoto) {
		this.workphoto = workphoto;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSmbid() {
		return smbid;
	}

	public void setSmbid(int smbid) {
		this.smbid = smbid;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpassword() {
		return userpassword;
	}

	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getUserroleid() {
		return userroleid;
	}

	public void setUserroleid(int userroleid) {
		this.userroleid = userroleid;
	}

	public int getCreateby() {
		return createby;
	}

	public void setCreateby(int createby) {
		this.createby = createby;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public int getModifyby() {
		return modifyby;
	}

	public void setModifyby(int modifyby) {
		this.modifyby = modifyby;
	}

	public Date getModifydate() {
		return modifydate;
	}

	public void setModifydate(Date modifydate) {
		this.modifydate = modifydate;
	}
}
