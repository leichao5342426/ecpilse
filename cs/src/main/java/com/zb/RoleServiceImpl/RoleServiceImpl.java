package com.zb.RoleServiceImpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zb.dao.RoleDao;
import com.zb.entity.Role;
import com.zb.service.RoleService;

public class RoleServiceImpl implements RoleService{

	public List<Role> findAllRoles() {
		List<Role> list=null;
		try {
			InputStream inputStream=Resources.getResourceAsStream("mybatis.cfg.xml");
			SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
			SqlSession session=sqlSessionFactory.openSession();
			RoleDao dao =session.getMapper(RoleDao.class);
			list=dao.findAllRoles();
			session.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	

}
