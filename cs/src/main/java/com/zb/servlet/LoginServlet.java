package com.zb.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.zb.entity.User;
import com.zb.service.UserService;
import com.zb.service.Impl.UserServiceImpl;

public class LoginServlet extends HttpServlet {
	private UserService service = new UserServiceImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String username = request.getParameter("username");
		String userpassword = request.getParameter("userpassword");
		User u = service.doLogin(username, userpassword);
		if (u != null) {
			//得到的是Session内置对象，这个永远不为空，除非服务器禁止session
			System.out.println("登录成功");
			
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", u);
			request.getRequestDispatcher("WEB-INF/jsp/frame.jsp").forward(request, response);
		} else {
			request.setAttribute("loginFlag", "error");
			request.getRequestDispatcher("index.jsp").forward(request, response);
			System.out.println("登录失败");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
