package com.zb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.zb.entity.Role;
import com.zb.service.RoleService;
import com.zb.service.Impl.RoleServiceImpl;

public class AjaxShowAllRoleServlet extends HttpServlet {
	
	private RoleService  service=new RoleServiceImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//查询所有角色列表
		List<Role> list=service.findAllRoles();
		//转json

		String json=JSONObject.toJSONString(list);
		//接受ajax传过来的值  response做响应
		
		response.setCharacterEncoding("utf-8");
		PrintWriter out=response.getWriter();
		out.print(json);
		out.flush();
		out.close();
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
