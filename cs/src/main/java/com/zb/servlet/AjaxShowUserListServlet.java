package com.zb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.zb.entity.User;
import com.zb.service.UserService;
import com.zb.service.Impl.UserServiceImpl;
public class AjaxShowUserListServlet extends HttpServlet {
	private UserService service=new UserServiceImpl(); 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//接值
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf8");
		String queryname=request.getParameter("queryname");
		String role =request.getParameter("role");
		int currentPage=Integer.parseInt(request.getParameter("currentPage"));
		//获取用户列表
		List<User> list=service.listUser(queryname, role, currentPage);
		//获取用户记录数
		int count =service.countUser(queryname, role);
		//获取总页数
		int totalPage =count%4==0 ? count/4:count/+1;
		Map<String, Object> param=new HashMap<String, Object>();
		param.put("list", list);
		param.put("count", count);
		param.put("totalPage", totalPage);
		//转json
		String json=JSONObject.toJSONString(param);
		PrintWriter out=response.getWriter();
		out.print(json);
		out.flush();
		out.close();
	  

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
