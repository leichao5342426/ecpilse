package com.zb.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.zb.entity.Role;
import com.zb.entity.User;
import com.zb.service.UserService;
import com.zb.service.Impl.UserServiceImpl;

public class UserAddServlet extends HttpServlet {
       private UserService service=new UserServiceImpl() ;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			User u = new User();
			//创建FileItemFactory对象
			FileItemFactory factory = new DiskFileItemFactory();
			//创建ServletFileupload对象
			ServletFileUpload upload = new ServletFileUpload(factory);
			//解析request
			List<FileItem> list = upload.parseRequest(request);
			for(FileItem fi : list){
				if(fi.isFormField()){
					//普通表单
					//首先获取name属性值
					String formName = fi.getFieldName();
					if("userCode".equals(formName)){
						u.setUsercode(fi.getString("utf-8"));
					}else if("userName".equals(formName)){
						u.setUsername(fi.getString("utf-8"));
					}else if("userPassword".equals(formName)){
						u.setUserpassword(fi.getString("utf-8"));
					}else if("gender".equals(formName)){
						u.setGender(Integer.parseInt(fi.getString("utf-8")));
					}else if("birthday".equals(formName)){
						String str = fi.getString("utf-8");
						//日期转化类   将字符串转成日期
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date d = sdf.parse(str);
						u.setBirthday(d);
					}else if("phone".equals(formName)){
						u.setPhone(fi.getString("utf-8"));
					}else if("address".equals(formName)){
						u.setAddress(fi.getString("utf-8"));
					}else if("userRole".equals(formName)){
						int roleid = Integer.parseInt(fi.getString("utf-8"));
						Role r = new Role();
						r.setId(roleid);
						u.setRole(r);
					}
				}else{
					//文件表单
					//获取应用服务器的根路径
					ServletContext application = this.getServletContext();
					String rootPath = application.getRealPath("/");
					System.out.println(rootPath);
					String dirName = "images";
					String dirPath = rootPath + dirName;
					File dirFile = new File(dirPath);
					if(!dirFile.exists()){
						//创建文件夹
						dirFile.mkdir();
					}
					
					//获取上传文件名
					String name = fi.getName();
					String photoPath = dirPath + "/" + name;
					//读取文件
					byte[] b = fi.get();
					FileOutputStream fos = new FileOutputStream(photoPath);
					fos.write(b);
					fos.flush();
					fos.close();
					
					String savePath = dirName + "/" + name;
					//首先获取name属性值
					String formName = fi.getFieldName();
					if("card".equals(formName)){
						
						u.setCardphoto(savePath);
					}else{
						
						u.setWorkphoto(savePath);
					}
					
				}
			}
			HttpSession session = request.getSession();
			User loginUser = (User) session.getAttribute("loginUser");
			//创建人id
			u.setCreateby(loginUser.getId());
			//创建时间
			u.setCreationdate(new Date());
			
			int count = service.addUser(u);
			if(count > 0){
				request.setAttribute("addFlag", "ok");
			}else{
				request.setAttribute("addFlag", "error");
			}
			request.getRequestDispatcher("WEB-INF/jsp/user/userlist.jsp").forward(request, response);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
